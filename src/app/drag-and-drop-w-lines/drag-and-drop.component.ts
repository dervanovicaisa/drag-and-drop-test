import { CdkDragDrop, CdkDragMove } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { DragAndDropService } from './service/drag-and-drop.service';

@Component({
  selector: 'app-drag-and-drop',
  templateUrl: './drag-and-drop.component.html',
  styleUrls: ['./drag-and-drop.component.css'],
})
export class DragAndDropWLinesComponent implements OnInit {
  node: Array<number> = new Array<number>;
  dots: Array<number> = [0, 1, 2, 3];
  pointerPosition: { x: number; y: number; } | undefined;
  distance: { x: number; y: number; } | undefined;
  pointCordinate: {} | undefined;
  id: number | undefined;
  dot: any;
  dotIndex: number | undefined;
  changed: boolean = false;
  constructor(private serviceDrop: DragAndDropService) { }
  ngOnInit(): void {
    this.serviceDrop.items.subscribe((el) => {
      this.node = el;
    })
  }
  onAddItem() {
    const itemRandom = +(Math.random() * 10).toPrecision(1);
    this.node.push(itemRandom);
    this.serviceDrop.onAddItem(this.node);
  }

  dotClicked(dot: HTMLDivElement, id: number) {
    this.dot = dot;
    const xCenter = (this.dot.getBoundingClientRect().left + this.dot.getBoundingClientRect().right) / 2;
    const yCenter = (this.dot.getBoundingClientRect().top + this.dot.getBoundingClientRect().bottom) / 2;
    this.pointCordinate = { x: xCenter, y: yCenter };
    this.dotIndex = id;
  }
  dragMoved(event: CdkDragMove, id: number) {
    if (this.dot !== undefined) {
      const xCenter = (this.dot.getBoundingClientRect().left + this.dot.getBoundingClientRect().right) / 2;
      const yCenter = (this.dot.getBoundingClientRect().top + this.dot.getBoundingClientRect().bottom) / 2;
      this.pointerPosition = event.pointerPosition;
      this.distance = event.distance;
      this.pointCordinate = { x: xCenter, y: yCenter };
      this.id = id;
      this.changed = false;
    }
  }
  dragDroped(event: any, id: number) {
    if (this.dot !== undefined) {
      const xCenter = (this.dot.getBoundingClientRect().left + this.dot.getBoundingClientRect().right) / 2;
      const yCenter = (this.dot.getBoundingClientRect().top + this.dot.getBoundingClientRect().bottom) / 2;
      this.pointerPosition = event.pointerPosition;
      this.distance = event.distance;
      this.pointCordinate = { x: xCenter, y: yCenter };
      this.id = id;
      this.changed = true;
    }
  }

}
