import { Input, OnDestroy } from '@angular/core';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { OnChanges, Component } from '@angular/core';

@Component({
  selector: 'lib-liner',
  template: `
  <svg #svg>
  <!-- <path #path did="curve" d="M0 0" stroke="green" stroke-width="4" stroke-linecap="round" fill="transparent" /> -->
  <path  #path  id="path" stroke="green" stroke-width="4" stroke-linecap="round" fill="transparent"  fill="none" d="M0 0"/>
  </svg>
  `,
  styleUrls: ['./line.style.css'],
})
export class LinerComponent implements OnChanges, OnDestroy {
  @Input("pointerPosition")
  pointerPosition: { x: number; y: number; } | undefined;
  @Input("distance")
  distance: { x: number; y: number; } | undefined;
  @Input("pointCordinate")
  pointCordinate: {} | undefined;
  @Input("id") id: number | undefined;
  @Input("dotIndex") dotIndex: number | undefined;
  @Input("changed") changed: boolean | undefined;
  @ViewChild("path") path: ElementRef;
  @ViewChild("svg") svg: ElementRef;
  constructor(path: ElementRef, svg: ElementRef) {
    this.path = path;
    this.svg = svg;
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges["dotIndex"] !== undefined) {
      localStorage.setItem("currValue", simpleChanges["dotIndex"].currentValue);
      localStorage.setItem("prevValue", simpleChanges["dotIndex"].previousValue);
    }
    this.moveAt();
    this.drawSvg();
  }

  moveAt() {
    if (this.dotIndex == 0) {
      localStorage.setItem("prevPointCordinate", JSON.stringify(this.pointCordinate));
    } else if (this.dotIndex == 1) {
      localStorage.setItem("currPointCordinate", JSON.stringify(this.pointCordinate));
    } else if (this.dotIndex == 2) {
      localStorage.setItem("prevPointCordinate", JSON.stringify(this.pointCordinate));
    } else if (this.dotIndex == 3) {
      localStorage.setItem("currPointCordinate", JSON.stringify(this.pointCordinate));
    }
    this.svg.nativeElement.style.top = 0 + 'px';
    this.svg.nativeElement.style.left = 0 + 'px';
  }
  absolute(x: number) {
    return (x < 0) ? -x : x;
  }
  signum(x: number) {
    return (x < 0) ? -1 : 1;
  }
  drawSvg() {

    const stroke = parseFloat(this.path.nativeElement.strokeWidth);

    const p1x = JSON.parse(Object(localStorage.getItem("prevPointCordinate"))).x;
    const p1y = JSON.parse(Object(localStorage.getItem("prevPointCordinate"))).y;

    const p2x = JSON.parse(Object(localStorage.getItem("currPointCordinate"))).x;
    const p2y = JSON.parse(Object(localStorage.getItem("currPointCordinate"))).y;

    if (this.svg.nativeElement.height < p2y) {
      this.svg.nativeElement.setAttribute("height", p2y);
    }
    if (this.svg.nativeElement.width < (p1x + stroke)) {
      this.svg.nativeElement.setAttribute("width", (p1x + stroke));
    }
    if (this.svg.nativeElement.width < (p2x + stroke)) {
      this.svg.nativeElement.setAttribute("width", (p2x + stroke));
    }
    const deltaX = (p2x - p1x) * 0.15;
    const deltaY = (p2y - p1y) * 0.15;

    const delta = deltaY < this.absolute(deltaX) ? deltaY : this.absolute(deltaX);
    let arc1 = 0;
    let arc2 = 1;

    if (p1x > p2x) {
      arc1 = 1;
      arc2 = 0;
    }


    const curve = `M ${p1x} ${p1y} V${p1y + delta} A${delta + " " + delta + " 0 0 " + arc1 + " " + (p1x + delta * this.signum(deltaX)) + " " + (p1y + 2 * delta)} H${(p2x - delta * this.signum(deltaX))} A${delta + " " + delta + " 0 0 " + arc2 + " " + p2x + " " + (p1y + 3 * delta)} V${p2y}`;

    this.path.nativeElement.setAttribute("d", curve);
    this.path.nativeElement.style.display = "block";

  }
  ngOnDestroy(): void {
    localStorage.removeItem("prevValue");
    localStorage.removeItem("currValue");
    localStorage.removeItem("prevPointCordinate");
    localStorage.removeItem("currPointCordinate");
  }
}
